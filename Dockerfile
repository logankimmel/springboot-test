FROM maven AS build
COPY src /usr/src/app/src  
COPY pom.xml /usr/src/app/  
RUN mvn -f /usr/src/app/pom.xml clean package

FROM adoptopenjdk:11-jre-hotspot as builder
WORKDIR /application
ARG JAR_FILE=/usr/src/app/target/*.jar
COPY --from=build ${JAR_FILE} application.jar
RUN java -Djarmode=layertools -jar application.jar extract

FROM adoptopenjdk:11-jre-hotspot
WORKDIR /application
COPY --from=builder application/dependencies/ ./
COPY --from=builder application/snapshot-dependencies/ ./
COPY --from=builder application/resources/ ./
COPY --from=builder application/application/ ./
ENTRYPOINT ["java", "org.springframework.boot.loader.JarLauncher"]